import React, { useState,useEffect } from "react";
import Link from 'next/link';
import Router from 'next/router'
import { Users, LogOut, Newspaper, Lightbulb, BookMarked, Home, Sparkles    } from 'lucide-react';

const SideBar =(props)=>{
    const [loginStatus, setLoginStatus] = useState(true);
    const onLogOut=()=>{
        localStorage.clear();
        setLoginStatus(false);
        Router.push('/login');
    }
    useEffect(() => {
        if(!localStorage.userid){
            Router.push('/login');

        }
        }, []);
    return (
        <>
               <nav className="side-nav">
             <Link href="#" className="intro-x flex items-center pl-5 pt-4"><img alt="" src={props.logo ? props.logo : 'http://122.160.48.132/dev/html/reseller-dashboard/images/logo.png'} /></Link>
                 <div className="side-nav__devider my-6"></div>
                     <ul>
                         <li>
                             <Link href="/dashboard" className="side-menu">
                                 <div className="side-menu__icon">
                                    {/* <i className="far fa-home-alt"></i> */}
                                    <Home size={16}/>
                                    </div>
                                 <div className="side-menu__title">Dashboard</div>
                             </Link>
                         </li>
                         <li>
                             <Link href="/projectList" className="side-menu side-menu--active"><div className="side-menu__icon">
                                {/* <i className="far fa-book"></i> */}
                                <BookMarked size={16}/>
                                </div>
                                 <div className="side-menu__title">Project details & Reports</div>
                             </Link>
                         </li>
  <li>
                             <Link href="/reseller-benefits" className="side-menu"><div className="side-menu__icon">
                                {/* <i className="fas fa-sparkles"></i> */}
                                <Sparkles  size={16}/>
                                </div>
                                 <div className="side-menu__title">Reseller Benefits</div>
                             </Link>
                         </li>
                       <li>
                         <Link href="#" className="side-menu"><div className="side-menu__icon">
                            {/* <i className="far fa-lightbulb-on"></i> */}
                            <Lightbulb size={16}/>
                            </div>
                             <div className="side-menu__title">Learning Resources</div>
                         </Link>
                       </li>
                         
                       <li className="side-nav__devider my-6"></li>
                         
                       
                   </ul>
             
                     <ul className="bottom-list">
                         <li>
                         <Link href="/profile" className="side-menu"><div className="side-menu__icon">
                            {/* <i className="far fa-address-card"></i> */}
                            <Newspaper size={16}/>
                            </div>
                             <div className="side-menu__title">Profile</div>
                         </Link>
                       </li>
                          <li>
                         <Link href="/users" className="side-menu"><div className="side-menu__icon">
                            {/* <i className="far fa-users"></i> */}
                            <Users size={16}/>
                            </div>
                             <div className="side-menu__title">Users</div>
                         </Link>
                       </li>
                          <li>
                         <Link href="#" onClick={onLogOut} className="side-menu"><div className="side-menu__icon">
                            {/* <i className="far fa-sign-out"></i> */}
                            <LogOut size={16}/>
                            </div>
                             <div className="side-menu__title">SignOut</div>
                         </Link>
                       </li>
                     </ul>
         </nav>
        </>
    )
}
export default SideBar;