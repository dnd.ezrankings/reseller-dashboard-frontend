import React, {useState, useEffect} from 'react'
import Head from 'next/head'

// import routes from './routes';
import Link from 'next/link';
import SideBar from './components/SideBar';
import TopHeader from './components/TopHeade';
import { CreditCard, Monitor, RefreshCw, ShoppingCart, User   } from 'lucide-react';


 const DashBoard = ()=> {
    useEffect(() => {
        const timer = setTimeout(() => {
        const ctx = document.getElementById('myChart');
    
        if (ctx) {
          const myChart = new Chart(ctx, {
            type: 'pie',
            data: {
              labels: [],
              datasets: [
                {
                  backgroundColor: ['#3498db', '#f1c40f', '#e74c3c'],
                  data: [50, 50, 50],
                },
              ],
            },
          });
        }

        'use strict';

        function applewatchgraph(box, value, _option) {
            if(box){
                var
                canvas = document.createElement('canvas'),
                context = canvas.getContext('2d'),
                boxsize = box.offsetWidth,
                option = _option || {},
                linewidth = option.linewidth || 24,
                barcolor = option.color || '#fff',
                noanimation = option.noanimation || false,
                from = option.from || 0,
                to = value,
                now = {percent: from},
                nowanimating = false,
                animateduration = 1,
                animateoptions = {ease: 'easeInOutCubic', onUpdate: onupdate, onComplete: onend};
                canvas.width = canvas.height = boxsize;
                box.appendChild(canvas);
                context.lineWidth = linewidth;
                context.lineCap = 'round';
                context.strokeStyle = barcolor;
                if (noanimation) {
                    option.autostart = true;
                }
                option.autostart ? start() : reset();
                function ready() {
                    draw(from);
                }
                function start() {
                    if (!nowanimating) {
                        now.percent = noanimation ? value : from;
                        if (now.percent == to) {
                            draw(now.percent);
                        } else {
                            nowanimating = true;
                            animateoptions.percent = to;
                            TweenMax.to(now, animateduration, animateoptions);
                        }
                    }
                }
                function onupdate() {
                    draw(now.percent);
                }
        
                function onend() {
                    nowanimating = false;
                }
        
                function draw(percent) {
                    var boxhalfsize = boxsize/2;
                    percent = Math.max(percent, 0.005);
                    context.clearRect(0, 0, boxsize, boxsize);
                    // bg
                    context.beginPath();
                    context.globalAlpha = 0.2;
                    context.arc(boxhalfsize, boxhalfsize, boxhalfsize-linewidth/2, 0, 2*Math.PI);
                    context.stroke();
                    // bar
                    context.beginPath();
                    context.arc(boxhalfsize, boxhalfsize, boxhalfsize-linewidth/2, 0-Math.PI/2, (2*Math.PI)*percent/100-Math.PI/2);
                    context.globalAlpha = 1;
                    context.stroke();
                }
                function reset() {
                    draw(from);
                }
                return {
                    start: start,
                    reset: reset
                }
            }
        }
        
        // group 1
        applewatchgraph(document.querySelector('#group1 .graph1'), 70, {
            color: '#ff180e',
            autostart: true
        });
        applewatchgraph(document.querySelector('#group1 .graph2'), 55, {
            color: '#9bfe07',
            autostart: true
        });
        applewatchgraph(document.querySelector('#group1 .graph3'), 26, {
            color: '#09daff',
            autostart: true
        });
        
        // group 2
        var graph21 = applewatchgraph(document.querySelector('#group2 .graph1'), 70, {
                color: '#ff180e',
                autostart: false
            });
        var graph22 = applewatchgraph(document.querySelector('#group2 .graph2'), 55, {
                color: '#9bfe07',
                autostart: false
            });
        var graph23 = applewatchgraph(document.querySelector('#group2 .graph3'), 26, {
                color: '#09daff',
                autostart: false
            });
            
            if(graph21 && graph21.start){
                
                setTimeout(graph21.start, 2000);

            }
            if(graph21 && graph22.start){
                setTimeout(graph22.start, 2500);

            }
            if(graph23 && graph23.start){
                setTimeout(graph23.start, 3000);

            }

        }, 1000);
        return () => clearTimeout(timer);
      }, []);
  const [faqData, setFaqData] = useState([]);
  const [readMore, setReadMore] = useState(false);
  const [readMoreClass, setReadMoreClass] = useState('hide');
  const updateContent=()=>{
   if(!readMore){
      setReadMore(true);
   }else{
      setReadMore(false);
   }
  }

  const [hiddenTitleIndex, setHiddenTitleIndex] = useState(0);
  const toggleHiddenTitle = (index) => {
    if (hiddenTitleIndex === index) {
      setHiddenTitleIndex(null);
    } else {
      setHiddenTitleIndex(index);
    }
  };
  return (
    <>
  <Head>
    <meta charset="utf-8" />
    <link href="#" rel="shortcut icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>
    <title>Reseller Dashboard</title>
    <link rel="dns-prefetch" href="//developers.google.com"/>
    <link rel="dns-prefetch" href="//maps.googleapis.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Nunito&amp;display=swap" rel="stylesheet" crossOrigin="anonymous" defer/>
    <link href="https://kit-pro.fontawesome.com/releases/v5.15.3/css/pro.min.css" rel="stylesheet" crossOrigin="anonymous" defer/>
    <script
    // you might need to get a newer version
    src="https://kit.fontawesome.com/fbadad80a0.js"
    crossOrigin="anonymous"
    defer
  ></script>    
    <script src="http://122.160.48.132/dev/html/reseller-dashboard/js/markerclusterer.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcUcow5QHjitBVOfkTdy44l7jnaoFzW1k&amp;libraries=places"></script>
    <script src="http://122.160.48.132/dev/html/reseller-dashboard/js/app.js"></script>
    <script src="http://122.160.48.132/dev/html/reseller-dashboard/js/map.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js'></script>  
    <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js"></script>
  </Head>
      
      <div className="flex mt-[4.7rem] md:mt-0">
        <SideBar />
        <div className="content">
        <TopHeader />
            <div className="grid grid-cols-12 gap-6">
                <div className="col-span-12 2xl:col-span-9">
                    <div className="grid grid-cols-12 gap-6">
                        <div className="col-span-12 mt-8">
                            <div className="intro-y flex items-center h-10">
                                <h2 className="text-lg font-medium truncate mr-5">Progress Meter</h2>
                                    <Link href="" className="ml-auto flex items-center text-primary">
                                        Show More
                                    </Link>
                            </div>
                        </div>
                        <div className="col-span-12 sm:col-span-6">
                            <div className="intro-y box p-5 apple-watch">
                                <div id="group1">
                                    <div className="graph1"></div>
                                    <div className="graph2"></div>
                                    <div className="graph3"></div>
                                </div>
                                <div className="w-52 sm:w-auto mx-auto">
                                <div className="flex items-center">
                                    <div className="w-2 h-2 bg-primary rounded-full mr-3 blue"></div>
                                    <span className="truncate">Billing - $1000</span>
                                </div>
                                <div className="flex items-center mt-4">
                                    <div className="w-2 h-2 bg-pending rounded-full mr-3 green"></div>
                                    <span className="truncate">Keywords - 300</span>
                                </div>
                                <div className="flex items-center mt-4">
                                    <div className="w-2 h-2 bg-warning rounded-full mr-3 red"></div>
                                    <span className="truncate">Projects - 100</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-span-12 sm:col-span-6">
                            <div className="intro-y box p-5 watch-display">
                                <div>
                                    <canvas id="myChart"></canvas>
                                </div>
                                <div className="w-52 sm:w-auto mx-auto">
                                    <div className="flex items-center">
                                        <div className="w-2 h-2 bg-primary rounded-full mr-3"></div>
                                        <span className="truncate">$1000- $10,000 - Silver</span>
                                    </div>
                                    <div className="flex items-center mt-4">
                                        <div className="w-2 h-2 bg-pending rounded-full mr-3"></div>
                                        <span className="truncate">$10,000 to $25000 - Gold</span>
                                    </div>
                                    <div className="flex items-center mt-4">
                                        <div className="w-2 h-2 bg-warning rounded-full mr-3"></div>
                                        <span className="truncate">$25000 & Above - Diamond</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="intro-y flex items-center h-10 mt-8">
                <h2 className="text-lg font-medium truncate mr-5">Reseller Benefits</h2>
                    <Link href="" className="ml-auto flex items-center text-primary">
                        {/* <i data-lucide="refresh-ccw" className="w-4 h-4 mr-3"></i> */}
                        <RefreshCw  className="w-4 h-4 mr-3"/>
                         Reload
                    </Link>
            </div>
            <div className="grid grid-cols-12 gap-6 mt-5">
                <div className="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <Link href="free-website-analysis.php">
                        <div className="report-box zoom-in">
                            <div className="box p-5">
                                <div className="flex">
                                    {/* <i data-lucide="shopping-cart" className="report-box__icon text-primary"></i> */}
                                    <ShoppingCart className="report-box__icon text-primary"/>
                                </div>
                                <div className="text-3xl font-medium leading-8 mt-6"><b>(1/10)</b></div>
                                <div className="text-base text-slate-500 mt-1">Free Website Analysis</div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <Link href="keyword-analysis.php">
                        <div className="report-box zoom-in">
                            <div className="box p-5">
                                <div className="flex">
                                    {/* <i data-lucide="credit-card" className="report-box__icon text-pending"></i> */}
                                    <CreditCard className="report-box__icon text-pending" />
                                </div>
                                <div className="text-3xl font-medium leading-8 mt-6"><b>(1/10)</b></div>
                                <div className="text-base text-slate-500 mt-1">Keywords Analysis</div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <Link href="free-festive-images.php">
                        <div className="report-box zoom-in">
                            <div className="box p-5">
                                <div className="flex">
                                    {/* <i data-lucide="monitor" className="report-box__icon text-warning"></i> */}
                                    <Monitor className="report-box__icon text-warning"/>
                                </div>
                                <div className="text-3xl font-medium leading-8 mt-6"><b>(1/10)</b></div>
                                <div className="text-base text-slate-500 mt-1">Free Festive Images</div>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <Link href="free-gmb-keywords.php">
                        <div className="report-box zoom-in">
                            <div className="box p-5">
                                <div className="flex">
                                    {/* <i data-lucide="user" className="report-box__icon text-success"></i> */}
                                    <User  className="report-box__icon text-success"/>
                                </div>
                                <div className="text-3xl font-medium leading-8 mt-6">(1/20)</div>
                                <div className="text-base text-slate-500 mt-1">Free GMB Keywords</div>
                            </div>
                        </div>
                    </Link>
                </div>
            </div> 
        </div>  
      </div>
    </>
  )
}


export default DashBoard;